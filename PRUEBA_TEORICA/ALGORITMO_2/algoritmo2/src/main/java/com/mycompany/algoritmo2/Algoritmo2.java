package com.mycompany.algoritmo2;

/**
 *
 * @author DELL
 */
public class Algoritmo2 {

    public static void main(String[] args) {
        /*
        Se tiene una X en la esquina superior izquierda de un área de 4x4. 
        Se tiene una matriz donde se podrá definir una cantidad ilimitada de elementos. 
        Cada 2 elementos de la matriz corresponden a un movimiento, 
        el primero en el eje horizontal y el segundo en el eje vertical. 
        El número indica las unidades a moverse y el signo la dirección (positivo para derecha o abajo, negativo para izquierda o arriba)
        Por ejemplo, para el arreglo myArray:=(1,2,-1,1,0,1,2,-1,-1,-2)
        La X se moverá una unidad a la derecha y dos hacia abajo, luego una unidad a la izquierda y una abajo 
        y así sucesivamente. El programa a escribir debe imprimir la posición final de la X. Para representar los lugares donde 
        la X no se encuentra utilizar la letra O. Si la instrucción obliga a la X a salir del área de 4x4 la X permanecerá en el borde, 
        sin salir. Para el arreglo presentado el resultado se vería así:

        */
        /*myArray:=(1,2,-1,1,0,1,2,-1,-1,-2)*/
        int[][] movimientos = {{1, 2}, {-1, 1}, {0, 1}, {2, -1}, {-1, -2}};
        char[][] celda = new char[4][4];
        // Inicializar la matriz con 'O'
        for(int i = 0; i < celda.length; i++) {
            for (int j = 0; j < celda[i].length; j++) {
                celda[i][j] = 'O';
            }
        }
        
        // Variables para simular el eje x y y
        int x = 0;
        int y = 0;

        // Realizar los movimientos del recorrido de la O
        for(int i = 0; i < movimientos.length; i++) {
            // Variables para determinar los movimientos
            int dx = movimientos[i][0];
            int dy = movimientos[i][1];
            // Actualizar las coordenadas de la X
            x += dx;
            y += dy;
            // Verificar si las coordenadas están dentro del área
            if (x < 0) {
                x = 0;
            } else if (x >= celda.length) {
                x = celda.length - 1;
            }
            // Para y
            if (y < 0) {
                y = 0;
            } else if (y >= celda[x].length) {
                y = celda[x].length - 1;
            }
            // Actualizar la matriz con la posición de la X
            celda[x][y] = 'X';
        }

        // Imprimir la matriz final
        for(int i = 0; i < celda.length; i++) {
            for (int j = 0; j < celda[i].length; j++) {
                System.out.print(celda[i][j]);
            }
            System.out.println();
        }
    }
    
    /*public static generarMovimientos(int[] myArray, char[] celda){
        
    }*/

}
